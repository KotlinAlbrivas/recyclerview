package com.albrivas.recyclerview

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.albrivas.recyclerview.model.MediaItem
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.startActivity

class MainActivity : AppCompatActivity() {

    var adapter = Adapter { navigateToDetail(it)}



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recycler.adapter = adapter
        MediaProvider.mediaAsync { adapter.listItems = it }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {


        /*when(item.itemId) {
            R.id.filter_all ->
                adapter.listItems = getMedia().let { media-> media}

            R.id.filter_photos ->
                adapter.listItems = getMedia().let { media -> media.filter { it.type == MediaItem.Type.PHOTO } }

            R.id.filter_videos ->
                adapter.listItems = getMedia().let { media -> media.filter { it.type == MediaItem.Type.VIDEO } }
        }*/

         /*MediaProvider.mediaAsync {media ->
             adapter.listItems = when(item.itemId) {
                R.id.filter_all -> media
                R.id.filter_photos -> media.filter { it.type == MediaItem.Type.PHOTO }
                R.id.filter_videos -> media.filter { it.type == MediaItem.Type.VIDEO }
                else -> emptyList()
            }
        }*/

        val filter: Filter = when(item.itemId) {
                R.id.filter_photos -> Filter.ByType(MediaItem.Type.PHOTO)
                R.id.filter_videos -> Filter.ByType(MediaItem.Type.VIDEO)
                else -> Filter.None
            }


        loadFilterData(filter)

        return true
    }

    private fun loadFilterData(filter: Filter) {
        MediaProvider.mediaAsync { media ->
            adapter.listItems = when (filter) {
                Filter.None -> media
                is Filter.ByType -> media.filter { it.type == filter.type }
            }
        }
    }

    private fun navigateToDetail(item: MediaItem) {
        startActivity<DetailActivity>(DetailActivity.ID to item.id )
    }

    sealed class Filter {
        object None: Filter()
        class ByType(val type: MediaItem.Type): Filter()
    }
}
