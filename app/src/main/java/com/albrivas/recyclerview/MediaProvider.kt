package com.albrivas.recyclerview

import com.albrivas.recyclerview.model.MediaItem
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

object MediaProvider {
    private val thumbBase = "http://lorempixel.com/400/400/sports/"

    private var data = emptyList<MediaItem>()

    fun mediaAsync(callback: (List<MediaItem>) -> Unit ) {

        doAsync {
            if(data.isEmpty()) {
                Thread.sleep(2000)

                data = (1..10).map {
                    MediaItem(it ,"Title $it", "$thumbBase$it", if (it % 3 == 0) MediaItem.Type.PHOTO else MediaItem.Type.VIDEO
                    )
                }
            }

            uiThread {
                callback(data)
            }
        }
    }
}

