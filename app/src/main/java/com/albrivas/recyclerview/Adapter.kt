package com.albrivas.recyclerview

import android.provider.MediaStore
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.albrivas.recyclerview.extensions.inflate
import com.albrivas.recyclerview.extensions.loadUrl
import com.albrivas.recyclerview.extensions.toast
import com.albrivas.recyclerview.model.MediaItem
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.view_media_item.view.*
import kotlin.properties.Delegates


class Adapter (items: List<MediaItem> = emptyList(),  val listener: (MediaItem) -> Unit):
    RecyclerView.Adapter<Adapter.ViewHolder>() {

    var listItems: List<MediaItem> by Delegates.observable(emptyList()) {
        _, _, _ -> notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflate = parent.inflate(R.layout.view_media_item)
        return ViewHolder(inflate)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = listItems[position]
        holder.bind(item)
        holder.itemView.setOnClickListener { listener(item) }
    }

    override fun getItemCount(): Int {
        return listItems.size
    }

    // Holder
    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bind(obj: MediaItem) = with(itemView) {
            media_title.text = obj.title
            media_thumb.loadUrl(obj.url)
            media_video_indicator.visibility = when(obj.type) {
                MediaItem.Type.PHOTO -> View.GONE
                MediaItem.Type.VIDEO -> View.VISIBLE
            }
        }
    }
}